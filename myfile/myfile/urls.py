from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'myfile.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^$', 'myfile.views.home'),
    url(r'^logout/$', 'myfile.views.logout'),
    url(r'^upload/', include('uploadfile.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/', include('registration.backends.simple.urls')),
    url(r'^tmpPage/', 'myfile.views.tmpPage'),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
