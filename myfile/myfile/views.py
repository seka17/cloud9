from django.contrib.auth.decorators import login_required
from django.shortcuts import render, render_to_response
from django.http import HttpResponseRedirect
from django.contrib import auth
from django.core.context_processors import csrf

# Create your views here.
from django.template import RequestContext
from myfile.forms import LoginForms
from uploadfile.models import UploadFile

def home(request):
    invalid = False
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = auth.authenticate(username=username, password=password)
        if user is not None:
            auth.login(request, user)
            return HttpResponseRedirect('upload/')
        else:
            invalid = True
    return render_to_response('home.html',
                                  {'form': LoginForms(),
                                   'invalid': invalid},
                                  RequestContext(request, {}))

@login_required
def logout(request):
    auth.logout(request)
    return HttpResponseRedirect('/')

def tmpPage(request):
    files = UploadFile.objects.filter(user=request.user)
    return render_to_response('tmpPage.html',
                                    {'files': files,
                                     'username': request.user},
                                    RequestContext(request, {}))