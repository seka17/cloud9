__author__ = 'seka'

from django.conf.urls import patterns, url
from uploadfile import views
urlpatterns = patterns('',
 url(r'^$', 'uploadfile.views.upload'),
 url(r'^upload1/$', 'uploadfile.views.upload1'),
 url(r'^upload2/$', 'uploadfile.views.upload2'),
 url(r'^download/(.+)/(.+)$', 'uploadfile.views.download'),
 url(r'^tmpurl/$', 'uploadfile.views.tmpfunc'),
 url(r'^tmpurl2/$', 'uploadfile.views.tmpfunc2'),
 url(r'^delete/$', 'uploadfile.views.delete'),
 url(r'^deleteDir/$', 'uploadfile.views.deleteDir'),
 url(r'^deleteLog/$', 'uploadfile.views.deleteLog'),
 )