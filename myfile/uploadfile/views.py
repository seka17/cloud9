# -*- coding: utf_8 -*-
import mimetypes
import urllib2
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
import os
from django.shortcuts import render, render_to_response
from django.http import HttpResponseRedirect, HttpResponse, StreamingHttpResponse
from django.template import RequestContext
from django.contrib import auth
from django.http import JsonResponse
from django.core.context_processors import csrf
# Create your views here.
from .models import UploadFile, Directories, LogEvent
from django.core.urlresolvers import reverse
from .forms import UploadFileForm, DirectoriesForm
from django.core.servers.basehttp import FileWrapper
from datetime import datetime
from django.core.exceptions import ObjectDoesNotExist
from myfile.settings import PROJECT_PATH


@login_required
def upload(request):
    """Универсальная функция, много чего делает."""
    """Сохранение файла."""
    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            f = form.save(commit=False)
            f.user = request.user
            f.pub_date = datetime.now()
            f.directory = Directories.objects.filter(id=request.session['curdir'])[0]
            f.save()
            try:
                log = LogEvent.objects.get(user=request.user)
            except LogEvent.DoesNotExist:
                log = LogEvent(user=request.user)
            log.upload_log += f.getFileName() + " was uploaded in " + str(f.pub_date) + " by " + str(log.user) + '\n'
            log.save()
            return HttpResponseRedirect(reverse('uploadfile.views.upload'))
    else:
        form = UploadFileForm()

    """Проверка, есть ли директории. Если нет, то создать директорию "default". """
    if len(Directories.objects.filter(user=request.user)) == 0:
        print "Directories are empty!"
        default = Directories(name="default", parent='', user=request.user)
        default.save()
        request.session['curdir'] = default.id
        try:
            log = LogEvent.objects.get(user=request.user)
        except LogEvent.DoesNotExist:
            log = LogEvent(user=request.user)
        log.dir_cr_log += default.name + " was created in " + str(datetime.now()) + " by " + \
                                str(log.user) + '\n'
        log.save()
    else:
        """Проверка на корректность указателя на начальную директорию.(Если пользователь другой зашел)"""
        try:
            if request.session['curdir'] < '1' or \
                            Directories.objects.filter(id=request.session['curdir'])[0].user != request.user:
                print "Another user!"
                request.session['curdir'] = Directories.objects.filter(name='default', user=request.user)[0].id
        except Exception:
            request.session['curdir'] = Directories.objects.filter(name='default', user=request.user)[0].id
        print request.session['curdir']
        default = Directories.objects.filter(id=request.session['curdir'])[0]
        print "Directories are not empty!"

    """Создание новой директории."""
    if request.method == 'POST':
        formDir = DirectoriesForm(request.POST)
        if formDir.is_valid():
            f = formDir.save(commit=False)
            f.parent = request.POST['parent']
            f.user = request.user
            f.save()
            try:
                log = LogEvent.objects.get(user=request.user)
            except LogEvent.DoesNotExist:
                log = LogEvent(user=request.user)
            log.dir_cr_log += f.name + " with parent " + f.parent + " was created in " + str(datetime.now()) + " by " + \
                                str(log.user) + '\n'
            log.save()

            return HttpResponseRedirect(reverse('uploadfile.views.upload'))
    else:
        formDir = DirectoriesForm()

    """Выбор текущей директории."""
    k = 0
    try:
        tmp = request.POST['dir']
        k = 1
    except Exception:
        pass
    if k == 0:
        currentDir = default
    else:
        currentDir = Directories.objects.filter(id=tmp)[0]
        request.session['curdir'] = tmp

    """Передача данных для отображения на сайте."""
    files = UploadFile.objects.filter(user=request.user, directory=currentDir)
    dirs = Directories.objects.filter(user=request.user)
    try:
        log = LogEvent.objects.get(user=request.user)
    except ObjectDoesNotExist:
        log = LogEvent(user=request.user)
        log.save()
    return render_to_response('upload.html', {'form': form,
                                              'username': request.user.username,
                                              'files': files,
                                              'dirs': dirs,
                                              'curDir': currentDir,
                                              'formDir': formDir,
                                              'log': log},
                              RequestContext(request, {})
    )


@login_required
def deleteLog(request):
    log = LogEvent.objects.get(user=request.user)
    log.delete()
    return HttpResponseRedirect(reverse('uploadfile.views.upload'))


def recdel(dir, username):
    """Рекурсивно удаляет директории и ихи содержимое."""
    for i in Directories.objects.filter(parent=dir.name):
        recdel(i)
    for j in UploadFile.objects.filter(user=username, directory=dir):
        filepath = j.getPath()
        if os.path.exists(filepath):
            os.remove(filepath)
        try:
            log = LogEvent.objects.get(user=username)
        except ObjectDoesNotExist:
            log = LogEvent(user=username)
        log.delete_log += j.getFileName() + " was deleted in " + str(datetime.now()) + " by " + \
                          str(log.user) + '\n'
        log.save()
        j.delete()

    try:
        log = LogEvent.objects.get(user=username)
    except LogEvent.DoesNotExist:
        log = LogEvent(user=username)
    log.dir_del_log += dir.name + " with parent " + dir.parent + " was deleted in " + str(datetime.now()) + " by " + \
                      str(log.user) + '\n'
    log.save()
    dir.delete()


@login_required
def deleteDir(request):
    dir = Directories.objects.filter(id=request.POST['dir_id'])[0]
    #if request.session['curdir'] == dir.id:
    for i in Directories.objects.all():
        if i.name == "default":
            request.session['curdir'] = i.id
            break
    recdel(dir, request.user)
    return HttpResponseRedirect(reverse('uploadfile.views.upload'))


@login_required
def delete(request):
    file = UploadFile.objects.filter(user=request.user).get(pk=request.POST['id'])
    logpath = os.path.join("myfile", "uploads", str(request.user), str(request.user) + '_log.txt')
    try:
        log = LogEvent.objects.get(user=request.user)
    except ObjectDoesNotExist:
        log = LogEvent(user=request.user)
    log.delete_log += file.getFileName() + "was deleted in " + str(datetime.now()) + " by " + \
                      str(log.user) + '\n'
    log.save()
    filepath = file.getPath()
    if os.path.exists(filepath):
        os.remove(filepath)
    file.delete()
    return HttpResponseRedirect(reverse('uploadfile.views.upload'))

'''def download(request, username, filename):
    path = os.path.join("myfile", "uploads", username, filename)
    if request.user.username == username and os.path.isfile(path):
        response = HttpResponse(FileWrapper(open(path)))
        #response['Content-Length'] = os.path.getsize(path)
        response['Content-Disposition'] = "attachment; filename=%s" % os.path.basename(path)
        return response
    else:
        return HttpResponseRedirect(reverse('uploadfile.views.upload'))
        '''


@login_required
def download(request, username, filename):
    path = os.path.join("myfile", "uploads", username, filename)
    if request.user.username == username and os.path.isfile(path):
        chunk_size = 8192
        response = StreamingHttpResponse(FileWrapper(open(path, 'rb'), chunk_size),
                                         content_type=mimetypes.guess_type(path)[0])
        response['Content-Length'] = os.path.getsize(path)
        response['Content-Disposition'] = "attachment; filename=%s" % os.path.basename(path).encode('ascii', 'ignore')
        path1 = path.encode('ascii', 'ignore')
        try:
            log = LogEvent.objects.get(user=request.user)
        except ObjectDoesNotExist:
            log = LogEvent(user=request.user)
        log.download_log += os.path.basename(path1) + " was downloaded in " + str(datetime.now()) + " by " + \
                            str(log.user) + '\n'
        log.save()
        return response
    else:
        return HttpResponseRedirect(reverse('uploadfile.views.upload'))


@login_required
def upload1(request):
    print "UPLOAD1"
    form = UploadFileForm()
    files = UploadFile.objects.filter(user=request.user)
    return render_to_response('upload1.html', {'form': form,
                                              'username': request.user.username,
                                              'files': files},
                              RequestContext(request, {}))


def tmpfunc(request):
    print "Got request"
    if request.method == 'POST':
        print "Request method is POST"
        form = UploadFileForm(request.POST, request.FILES)
        print "Successfully uploaded"
        if form.is_valid():
            print "Form is valid"
            f = form.save(commit=False)
            f.user = request.user
            f.pub_date = datetime.now()
            f.save()
            form = UploadFileForm()
            files = UploadFile.objects.filter(user=request.user)
            response = render_to_response('tmpPage.html', {'form': form,
                                              'username': request.user.username,
                                              'files': files},
                              RequestContext(request, {}))
            return response


@login_required
def upload2(request):
    form = UploadFileForm()
    files = UploadFile.objects.filter(user=request.user)
    return render_to_response('upload2.html', {'form': form,
                                              'username': request.user.username,
                                              'files': files},
                              RequestContext(request, {}))


def tmpfunc2(request):
    print "Got request"
    if request.is_ajax():
        print "Request method is POST"
        form = UploadFileForm(request.POST, request.FILES)
        print "Successfully uploaded"
        if form.is_valid():
            print "Form is valid"
            f = form.save(commit=False)
            f.user = request.user
            f.pub_date = datetime.now()
            f.save()
            form = UploadFileForm()
            files = UploadFile.objects.filter(user=request.user)
            #print "Before Response"
            #c = {'files': list(files)}
            #print c
            #response = JsonResponse(c, safe=False)
            response = render_to_response('tmpPage.html', {'form': form,
                                              'username': request.user.username,
                                              'files': files},
                              RequestContext(request, {}))
            #print "After Response"
            return response