__author__ = 'seka'

from django import forms
from models import UploadFile, Directories


class UploadFileForm(forms.ModelForm):

    class Meta:
        model = UploadFile
        fields = ['name', 'comment', 'docfile']


class DirectoriesForm(forms.ModelForm):

    class Meta:
        model = Directories
        fields = ['name']
