import os
from django.db import models
from django.core.validators import RegexValidator
from django.contrib.auth.models import User
import urllib2
from time import time
# Create your models here.
alphanumeric = RegexValidator(r'^[0-9a-zA-Z. ]*$', 'Only alphanumeric characters are allowed.')

"""
def get_upload_file_name(instance, filename):
    return "%s_%s" % ( str(time()).replace('.', '_'), filename)


class Data(models.Model):
    name = models.CharField(max_length=50, validators=[alphanumeric])
    pub_date = models.DateTimeField('date uploaded')
    user = models.CharField(max_length=50, validators=[alphanumeric])
    comment = models.TextField()
    def __unicode__(self):
        return self.name
    f = models.FileField(upload_to="%s/%s_%s" % ("sdasd", str(time()).replace('.', '_'), "sdasf"))
"""


def file(self, filename):
    return "%s/%s" % (self.user.username, filename)


class LogEvent(models.Model):
    user = models.ForeignKey(User)
    upload_log = models.TextField()
    download_log = models.TextField()
    delete_log = models.TextField()
    dir_cr_log = models.TextField()
    dir_del_log = models.TextField()
    #file = models.ForeignKey(UploadFile)

class Directories(models.Model):
    user = models.ForeignKey(User)
    name = models.CharField(max_length=256, validators=[alphanumeric])
    parent = models.CharField(max_length=256, validators=[alphanumeric])

    def __unicode__(self):
        return self.name


class UploadFile(models.Model):
    name = models.CharField(max_length=256, validators=[alphanumeric])
    pub_date = models.DateTimeField('date uploaded')
    comment = models.TextField()
    docfile = models.FileField(upload_to=file)
    user = models.ForeignKey(User)
    directory = models.ForeignKey(Directories)
    #directory = models.ForeignKey(Directories)

    def getPath(self):
        return urllib2.unquote("myfile" + self.docfile.url).decode('utf8')

    def getFileName(self):
        return os.path.basename(self.getPath())


